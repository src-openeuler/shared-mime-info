Name:	    shared-mime-info	
Version:	2.4
Release:	2
Summary:	Shared MIME information database
License:	GPL-2.0-or-later
URL:		https://freedesktop.org/wiki/Software/shared-mime-info/
Source0:	https://gitlab.freedesktop.org/xdg/shared-mime-info/-/archive/%{version}/shared-mime-info-%{version}.tar.gz
Source1:    mimeapps.list

Patch0:     0001-Remove-sub-classing-from-OO.o-mime-types.patch
Patch1:     0002-Fix-build-with-libxml2-2.12.0.patch

BuildRequires: gcc gcc-c++ gettext intltool perl-XML-Parser meson itstool xmlto
BuildRequires: pkgconfig(gio-2.0)
BuildRequires: pkgconfig(glib-2.0) >= 2.6.0
BuildRequires: pkgconfig(libxml-2.0) >= 2.4

%global __requires_exclude ^/usr/bin/pkg-config$

%description
The shared-mime-info package contains the core database of common types 
and the update-mime-database command used to extend it. It requires 
glib2 to be installed for building the update command. Additionally, it 
uses intltool for translations, though this is only a dependency for 
the maintainers.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1 

%build
%meson
%meson_build

%install
%meson_install

find $RPM_BUILD_ROOT%{_datadir}/mime -type d \
| sed -e "s|^$RPM_BUILD_ROOT|%%dir |" > %{name}.files
find $RPM_BUILD_ROOT%{_datadir}/mime -type f -not -path "*/packages/*" \
| sed -e "s|^$RPM_BUILD_ROOT|%%ghost |" >> %{name}.files

install -d  $RPM_BUILD_ROOT%{_datadir}/applications
install -m 644 %SOURCE1 $RPM_BUILD_ROOT/%{_datadir}/applications/mimeapps.list

# remove bogus translation files
# translations are already in the xml file installed
rm -rf $RPM_BUILD_ROOT%{_datadir}/locale/*

%check
%meson_test

%post
/bin/touch --no-create %{_datadir}/mime/packages &>/dev/null ||:

%transfiletriggerin -- %{_datadir}/mime
update-mime-database -n %{_datadir}/mime &> /dev/null ||:

%transfiletriggerpostun -- %{_datadir}/mime
update-mime-database -n %{_datadir}/mime &> /dev/null ||:

%files
%license COPYING
%{_bindir}/update-mime-database
%{_datadir}/applications/*.list
%{_datadir}/mime/packages/*.org.xml
%dir %{_datadir}/pkgconfig
%{_datadir}/pkgconfig/shared-mime-info.pc
%dir %{_datadir}/gettext
%dir %{_datadir}/gettext/its
%{_datadir}/gettext/its/shared-mime-info.its
%{_datadir}/gettext/its/shared-mime-info.loc

%files help
%doc README.md NEWS HACKING.md data/shared-mime-info-spec.xml
%{_mandir}/man1/*

%changelog
* Wed Aug 07 2024 Funda Wang <fundawang@yeah.net> - 2.4-2
- Remove locale files rather than exclude them
- fix build with libxml 2.12

* Fri Feb 02 2024 zhouwenpei <zhouwenpei1@h-partners.com> - 2.4-1
- Upgrade to 2.4

* Sat Oct 29 2022 wangkerong <wangkeorng@h-partners.com> - 2.2-1
- Upgrade to 2.2

* Mon Jun 20 2022 hanhui <hanhui15@h-partners.com> - 2.1-2
- migrate from custom itstool to builtin msgfmt for creating translated XML

* Mon Feb 1 2021 jinzhimin <jinzhimin2@huawei.com> - 2.1-1
- Upgrade to 2.1

* Thu Sep 10 2020 hanhui <hanhui15@huawei.com> - 2.0-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:modify source url

* Fri Jul 31 2020 chxssg<chxssg@qq.com> - 2.0-1
- Update to 2.0

* Tue Aug 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.10-4
- Package init
